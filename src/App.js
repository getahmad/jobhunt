import Routes from "./routes";
import { ConfigProvider } from "antd";
import themeCustom from "./utils/themeCustom";
import "antd/dist/reset.css";
import "./App.css"
function App() {
  return (
    <ConfigProvider theme={themeCustom}>
      <Routes />
    </ConfigProvider>
  );
}

export default App;
