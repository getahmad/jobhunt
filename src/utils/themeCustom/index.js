const themeCustom = {
  token: {
    fontFamily: "Epilogue",
  },
  components: {
    Button: {
      colorPrimary: "#4640DE",
      colorPrimaryHover: "#4970DE",
      controlHeight: 50,
      controlHeightLG: 57,
      controlHeightSM: 42,
      fontSize: 16,
      fontSizeLG: 18,
      paddingXS: 10,
      paddingContentHorizontal: 21,
      borderRadius: 0,
      borderRadiusLG: 0,
      borderRadiusSM: 0,
      //   secondary
      colorBorder: "#CCCCF5",
      colorText: "#4640DE",
      colorLink: "#4640DE",
    },
    Typography: {
      fontSize: 16,
      fontSizeLG: 18,
      colorText: "#25324B",
      fontSizeHeading1: 72,
      fontSizeHeading2: 48,
      fontSizeHeading3: 32,
      fontSizeHeading4: 24,
      fontSizeHeading5: 20,
    },
    Input: {
      colorBorder: "#D6DDEB",
      colorPrimary: "#4640DE",
      colorError: "#FF6550",
      controlHeight:50,
      controlPaddingHorizontal:16,
      borderRadius:0,
      colorText:"#515B6F"
    },
  },
};

export default themeCustom;
