import React from "react";
import { BrowserRouter, Routes as Router, Route } from "react-router-dom";
import publicRoute from "./public";
const Routes = () => {
  return (
    <BrowserRouter>
      <Router>
        {publicRoute.map((data) => {
          return <Route path={data.path} key={data.key} element={data.element} />;
        })}
      </Router>
    </BrowserRouter>
  );
};

export default Routes;
