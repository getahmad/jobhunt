import Home from "../pages/home";
import Login from "../pages/login";

const publicRoute = [
  {
    key: "home",
    path: "/",
    element: <Home />,
  },
  {
    key: "login",
    path: "/login",
    element: <Login />,
  },
];

export default publicRoute;
