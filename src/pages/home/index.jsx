import React from "react";
import { Typography, Button, Input, Space } from "antd";
import { PlusOutlined, ArrowRightOutlined,SearchOutlined } from "@ant-design/icons";
const { Title, Text } = Typography;

export default function Home() {
  return (
    <div>
      {/* HEADING */}
      <Title>h1. Ant Design</Title>
      <Title level={2}>h2. Ant Design</Title>
      <Title level={3}>h3. Ant Design</Title>
      <Title level={4}>h4. Ant Design</Title>
      <Title level={5}>h5. Ant Design</Title>
      <hr />
      {/* Body */}
      <Space direction="vertical">
        <Text className="body-xlarge">body xlarge</Text>
        <Text className="body-large-bold">body large bold</Text>
        <Text className="body-large-semibold">body large semibold</Text>
        <Text className="body-large-medium">body large medium</Text>
        <Text className="body-large-regular">body large regular</Text>
        <Text className="body-normal-bold">body normal bold</Text>
        <Text className="body-normal-semibold">body normal semibold</Text>
        <Text className="body-normal-medium">body normal medium</Text>
        <Text className="body-normal-regular">body normal regular</Text>
        <Text className="body-small-semibold">body small regular</Text>
        <Text className="body-small-regular">body small regular</Text>
      </Space>
      <hr />
      {/* DISPLAY */}
      <Text className="display1">display 1</Text> <br />
      <Text className="display2">display 2</Text> <br />
      <Text className="display3">display 3</Text> <br />
      <hr />
      {/* BUTTON */} <br />
      <Button type="primary" size="small" className="body-large-bold">
        Primary
      </Button>
      <Button type="primary" className="body-normal-bold">
        Primary
      </Button>
      <Button type="primary" size="large" className="body-large-bold">
        Primary
      </Button>
      <Button
        type="primary"
        icon={<PlusOutlined />}
        size="small"
        className="body-large-bold"
      >
        Primary
      </Button>
      <Button
        type="primary"
        icon={<PlusOutlined />}
        className="body-normal-bold"
      >
        Primary
      </Button>
      <Button
        type="primary"
        icon={<PlusOutlined />}
        size="large"
        className="body-large-bold"
      >
        Primary
      </Button>
      <Button
        type="primary"
        size="small"
        icon={<ArrowRightOutlined />}
        className="body-large-bold"
      />
      <Button
        type="primary"
        icon={<ArrowRightOutlined />}
        className="body-normal-bold"
      />
      <Button
        type="primary"
        size="large"
        icon={<ArrowRightOutlined />}
        className="body-large-bold"
      />
      <br />
      <Button size="small" className="body-large-bold">
        Caption
      </Button>
      <Button className="body-normal-bold">Caption</Button>
      <Button size="large" className="body-large-bold">
        Caption
      </Button>
      <Button icon={<PlusOutlined />} size="small" className="body-large-bold">
        Caption
      </Button>
      <Button icon={<PlusOutlined />} className="body-normal-bold">
        Caption
      </Button>
      <Button icon={<PlusOutlined />} size="large" className="body-large-bold">
        Caption
      </Button>
      <Button
        size="small"
        icon={<ArrowRightOutlined />}
        className="body-large-bold"
      />
      <Button icon={<ArrowRightOutlined />} className="body-normal-bold" />
      <Button
        size="large"
        icon={<ArrowRightOutlined />}
        className="body-large-bold"
      />
      <br />
      <Button type="link" size="small" className="body-large-bold">
        Link
      </Button>
      <Button type="link" className="body-normal-bold">
        Link
      </Button>
      <Button type="link" size="large" className="body-large-bold">
        Link
      </Button>
      <Button
        type="link"
        icon={<PlusOutlined />}
        size="small"
        className="body-large-bold"
      >
        Link
      </Button>
      <Button type="link" icon={<PlusOutlined />} className="body-normal-bold">
        Link
      </Button>
      <Button
        type="link"
        icon={<PlusOutlined />}
        size="large"
        className="body-large-bold"
      >
        Link
      </Button>
      <Button
        className="body-small-bold"
        size="small"
        type="link"
        icon={<ArrowRightOutlined />}
      />
      <Button
        type="link"
        icon={<ArrowRightOutlined />}
        className="body-normal-bold"
      />
      <Button
        size="large"
        type="link"
        icon={<ArrowRightOutlined />}
        className="body-large-bold"
      />
      <hr />
      {/* BUTTON */}
      <Input placeholder="Basic usage" />
      <Input placeholder="default size" prefix={<SearchOutlined style={{ color:"#A8ADB7" }} />} />
      <Input placeholder="Basic usage" status="error" />
      <Input placeholder="default size" status="error"   prefix={<SearchOutlined style={{ color:"#A8ADB7" }} />} />
    </div>
  );
}
