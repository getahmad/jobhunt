import React from "react";
import { useNavigate } from "react-router-dom";
export default function Login() {
  const navigate = useNavigate();
  const goToHome = () => {
    return navigate("/");
  };

  return (
    <div>
      <h1>ini login page</h1>
      <button onClick={goToHome}>got to home</button>
    </div>
  );
}
